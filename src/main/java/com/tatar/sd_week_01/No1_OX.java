/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tatar.sd_week_01;
import java.io.*;
import java.util.*;
/**
 * @62160007 Thanawat Thongtitcharoen
 * @author Dell
 */
public class No1_OX {
    public static void main(String [] args) throws IOException{ //***Note this program X=1 , O=2 and 0="-" in array
		Scanner kb = new Scanner(System.in);        //import input from keyboard
		System.out.println("Welcome to OX Game");   
	        
            int arr [][] = {{0,0,0},{0,0,0},{0,0,0}};       //variable   
	    int countround=0;
	    String rol = "";
	    String col = "";
	    int Crol;
	    int Ccol;
	    int report=0;
	    boolean Stopper=true;
	    while(Stopper) {                                //loop for run game
		checkPrint(arr);                            //print table
		
		Turn(countround);                           //print X or O turn
		
		System.out.println("Please input Row Col: ");   
		
		int checkIn=0;                          
		while(checkIn==0) {                         //loop for run input and check user error
			rol=kb.next();                      //input rol from keyboard
			col=kb.next();                      //input col from keyboard
			checkIn=checkRolColInput(rol, col); //call method check input number 1 to 3
			if(checkIn==0) {                    //if for check report from check input method and tell user is not input number 1 to 3
				System.out.println("Something Wrong, Please Input Number Only! ");
				System.out.println("Please input Row Col: ");
			}
			else {                              //else for check report from check input method and tell user can't input this location
				checkIn=checkRolColLocation(rol, col, arr);
				if(checkIn==2) {
				System.out.println("This Location is already, Please input other Location again! ");
				System.out.println("Please input Row Col: ");
				checkIn=0;
				}
			}
			
		}
		Crol=Conv(rol);                             //convert input from String to integer
		Ccol=Conv(col);
		
		arr[Crol][Ccol]=pullTurn(countround);       //set X or O loaction in table
		
		
		report+=BingX(arr);                         //check X or O Bing
		report+=BingY(arr);
		report+=BingCrossA(arr);
		report+=BingCrossB(arr);
		if(report!=0) {                             //tell user X or O win
			checkPrint(arr);
			System.out.println("Player "+pullReport(report)+" Win....");
			System.out.println("Bye bye ....");
			break;
		}
		
		
		if(countround==8) {                         //tell user no winner
			Stopper=false;
			checkPrint(arr);
			System.out.println("Game Over on Winner....");
			System.out.println("Bye bye ....");
		}
		countround++;                               //count round
	    }
     
	}
	public static String pullReport(int XO) {           //for convert number to String X or O
		if(XO==1) { 
			return "X";
		}
		return "O";
	}
	public static int BingX(int arr[][]) {              //check X or O Bing in X axis
		for(int i=0;i<3;i++) {
			for(int j=1;j<=2;j++) {
				if(arr[i][0]==j&&arr[i][1]==j&&arr[i][2]==j) {
					return j;
				}
			}
		}
		return 0;
	}
	public static int BingY(int arr[][]) {              //check X or O Bing in Y axis
		for(int i=0;i<3;i++) {
			for(int j=1;j<=2;j++) {
				if(arr[0][i]==j&&arr[1][i]==j&&arr[2][i]==j) {
					System.out.println(arr[0][i]+" "+arr[1][i]+" "+arr[2][i]+" "+i+" "+j);
					return j;
				}
			}
		}
		return 0;
	}
	public static int BingCrossA(int arr[][]) {         //check X or O Bing in cross 
		for(int i=1;i<=2;i++) {
			if(arr[0][0]== i && arr[1][1]== i &&arr[2][2]== i) {
				return i;
			}
		}
		return 0;
	}
	public static int BingCrossB(int arr[][]) {         //check X or O Bing in cross 
		for(int i=1;i<=2;i++) {
			if(arr[2][0]== i && arr[1][1]== i &&arr[0][2]== i) {
				return i;
			}
		}
		return 0;
	}
	
	public static int pullTurn(int countround) {        //tell this turn is turn of X or O (not for show user)
		if(countround%2==0) {
			return 1;
		}
		else {
			return 2;
		}
	}
	public static void Turn(int countround) {           //show X or O turn for show user
		if(countround%2==0) {
			System.out.println("X turn");
		}
		else {
			System.out.println("O turn");
		}
	}
	public static int Conv(String rol) {                //convert String to integer method
		if(rol.equals("1")) {
			return 0;
		}
		else if(rol.equals("2")) {
			return 1;
		}
		else if(rol.equals("3")) {
			return 2;
		}
		return 0;
	}
	
	
	public static int checkRolColInput(String rol,String col) {                 //method for check input number 1 to 3
		if((rol.equals("1")||rol.equals("2")||rol.equals("3"))&&(col.equals("1")||col.equals("2")||col.equals("3"))) {
			return 1;
		}
		
		return 0;
	}
	
	public static int checkRolColLocation(String rol,String col,int arr[][]) {  //method for check location is empty for input (Yes or No) 
		int Crol=Conv(rol);
		int Ccol=Conv(col);
		if(arr[Crol][Ccol]==0) {
			return 1;
		}
		
		return 2;
	}
	
	public static void checkPrint(int arr[][]) {        //method for print game and location of X and O table
		System.out.println("  1 2 3");
		for(int i=0;i<3;i++) {
			System.out.print(i+1+" ");
			for(int j=0;j<3;j++) {
				if(arr[i][j]==0) {
					System.out.print("- ");
				}
				else {
					cDecode(arr[i][j]);
				}
			}
			System.out.println("");
		}
	}
	
	public static void cDecode(int a) {                 //for convert number to String and show user X or O
        if(a==1) {
        	System.out.print("X ");
        }
        else if(a==2) {
        	System.out.print("O ");
        }
	}
}
